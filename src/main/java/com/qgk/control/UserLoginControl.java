package com.qgk.control;

import com.qgk.bean.UserInfoBean;
import com.qgk.services.IUserInfoService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.io.File;
@Controller
public class UserLoginControl {


    @Resource
    private IUserInfoService userInfoService;



    /*
映射关系：要求实体类的属性名和前台页面的表单控件的name属性值一一对应
课堂作业：
   如果使用 @RequestParam 对方法参数进行绑定，是否可行？
*/
    @RequestMapping("/login4")
    public ModelAndView doLogin4(UserInfoBean userInfoB) {
       // System.out.println("basepath:" + System.getProperty("server.basePath"));
        // 返回的视图对象
        ModelAndView mav= new ModelAndView();
        // 设置modelandview要显示的页面名:默认进入登录成功页面
        mav.setViewName("logined");

        if (userInfoB != null) {
            System.out.println("后台登录处理：" + userInfoB.getUsername() + ":" + userInfoB.getUserpsw());
        } else {
            System.out.println("后台登录：null");
        }
        // 调用service的接口方法，进行登录的业务逻辑处理
        List<UserInfoBean> lstUserInfos = userInfoService.doLogin(userInfoB);

        // 登录判定
        if (lstUserInfos != null && lstUserInfos.size() > 0) {
            // 登录成功，缓存当前登录成功的数据
            // 课堂作业：ModelAndView的缓存级别对应的是那个？ request??????  ModelAndView 的数据写在request里，如果是想存用户信息，比如登录人信息，可以存入session中
            mav.addObject("loginedUserName", lstUserInfos.get(0).getUsername());


        } else {
            // 登录不成功
            System.out.println("登录失败");
            // 返回登录页面
            mav.setViewName("../../index"); //注意:springMVC进行视图解析时,不会处理../,而是直接拼接字符串
            //使用字符串方式作为响应函数的返回值
            //是否还有其他的解决方案???

        }

        // 返回ModelAndView对象
        return mav;

    }


/**
 * 保存和修改用于信息
 *
 * @param userInfoBean 用户参数实体类
 * @param file_userimg 用于接收客户端发送的流文件对象
 * @return 结果视图
 */
    //@RequestParam("file_userimg")
    @RequestMapping("/saveOrUpdateUserInfo")
    public String saveOrUpdateUserInfo(UserInfoBean userInfoBean,@RequestParam("file_userimg")MultipartFile file_userimg){


        // springmvc将form表单中的file控件的流文件内容映射到 MultipartFile 形参变量file_userimg上
        System.out.println("getOriginalFilename():" + file_userimg.getOriginalFilename());
        System.out.println("getSize():" + file_userimg.getSize());
        System.out.println("getContentType():" + file_userimg.getContentType());
        System.out.println("getName():" + file_userimg.getName());


        // 将文件对象file_userimg 中的文件流上传到服务器
        // 获取文件的服务器的保存路径
        System.out.println("basepath:" + System.getProperty("server.basePath"));
        // 当前项目在web服务器上的部署的绝对路径
        String serverBasePath = System.getProperty("server.basePath");

        // 设置服务器的保存文件的路径名
        String serverFileSavePath = "uploadfiles/userimgfiles/";
        // 完整的服务器保存路径为 serverBasePath+ serverFileSavePath
        String serverSavePath = serverBasePath + serverFileSavePath;
        // 验证服务器是否已经创建了该目录

        File fileSave = new File(serverSavePath);
        // 判断保存路径文件对象是否存在
        if (!fileSave.exists()) {
            // 创建该目录
            fileSave.mkdirs();
        }

        // 文件需要在服务器上进行重命名，然后进行保存
        // 命名规则： 前缀+系统时间毫秒数+文件后缀
        // 获取文件类型
        String fileContentType = file_userimg.getContentType();
        // 获取文件后缀
        String fileType = fileContentType.substring(fileContentType.indexOf("/") + 1);
        // 文件名前缀
        String fileNamePrefix = "userimg";
        // 生成新的文件名
        String fileNewName = fileNamePrefix + System.currentTimeMillis() + "." + fileType;
        System.out.println("新文件名：" + fileNewName);
        // 将文件流写出到服务器保存路径
        try {
            file_userimg.transferTo(new File(serverSavePath + fileNewName));
        } catch (IOException e) {
            System.out.println("文件上传异常：" + e.getMessage());
            e.printStackTrace();
        }
        // 需要保存的文件的访问路径
        String fileRelativePath = serverFileSavePath + fileNewName;
        System.out.println("文件的访问路径：" + fileRelativePath);
        // 将文件对象的访问路径映射给实体类对象的成员属性上
        userInfoBean.setUserimg(fileRelativePath);
        // 使用反射，将设置属性的方法具有通用性
        // 提取需要保存的属性名
        String propertyName = file_userimg.getName().replace("file_", "");
        try {
            BeanUtils.setProperty(userInfoBean, propertyName, fileRelativePath);
        } catch (Exception e) {
            System.out.println("文件上传异常：" + e.getMessage());
            e.printStackTrace();
        }
        // 调用Service层进行用户信息数据保存和修改操作
        int i = userInfoService.saveOrUpdateUserInfo(userInfoBean);
        // 根据操作结果进行视图转发处理
        return "userInfoManager/userInfoAddDetail";
    }

}
